# FASER DTO

FASER scripts for copying data to tape, based on top of the DTO package from EP-DT by Marco Boretto
  https://gitlab.cern.ch/ep-dt-di/daq/dto

Files are copied to EOS and CTA and removed from the daq disk after one day if the files
have been transferred successfully to tape. The transactions are recorded in an mysql database.

By default only files on /data owned by the shifter account and following the FASER naming scheme
for raw data files are copied. Destinations are given in ".env"

## Checking location of files

Files should go to the disk area:
```
/eos/experiment/faser/raw/{year}/{runno}
```
and will also be copied to taped at:
```
/eos/ctapublic/archive/faser/raw/{year}/{runno}
```
The year has to be changed at the start of each year in the .env file.

One can check the tape status with:
```
EOS_MGM_URL=root://eosctapublic.cern.ch eos ls -y /eos/ctapublic/archive/faser/raw/{year}/{runno}
```


## information on how CTA works

An overview of how the storage to CTA works can be found at:
[CTA Overview](https://indico.cern.ch/event/985953/contributions/4238328/attachments/2200409/3721499/EOS%2BCTA_WorkFlows.pdf)
with detailed instructions at:
[Copying to CTA instructions](https://cern.service-now.com/service-portal?id=kb_article&n=KB0007266) and
[Copying from CTA instructions](https://cern.service-now.com/service-portal?id=kb_article&n=KB0007167).

Only the `faserdaq` account has write and retrieval rights to the CTA area. 
Retrieval still needs to be tested beyond a single file.

## Installation instructions

First install the general faserdaq certificate and the specific host certificates
```
mkdir /etc/robot-certificate
cd /etc/robot-certificate
openssl pkcs12 -in <path>/faserdaqCertificate.p12 -nocerts -nodes > userkey.pem
opnssl pkcs12 -in <path>/faserdaqCertificate.p12 -nokeys -nodes > usercert.pem
chmod 0600 user*

mkdir /etc/grid-security
cd /etc/grid-security
openssl pkcs12 -in <path>/faser-daq-011.p12 -nocerts -nodes > hostkey.pem
openssl pkcs12 -in <path>/faser-daq-011.p12 -nokeys -nodes > hostcert.pem
chmod 0600 host*
```
Setup by automatic renewal (needs certificate packages below first, though):
```
crontab -e
#add this at end:
0 */4 * * * voms-proxy-init --cert /etc/robot-certificate/usercert.pem --key /etc/robot-certificate/userkey.pem -hours 24 -q
```

Note that these certificates need to be renewed once per year and there is no automatic warning for the robot certificate, only the host one.

Next the gridftp server will be installed for transmitting the datafiles out of the DAQ server (note http proxy variables need to be set for dnf to work properly):
```
echo "# EGI Software Repository - REPO META (releaseId,repositoryId,repofileId) - (12655,-,2283)

[EGI-trustanchors]
name=EGI-trustanchors
baseurl=http://repository.egi.eu/sw/production/cas/1/current/
enabled=1
gpgcheck=1
gpgkey=http://repository.egi.eu/sw/production/cas/1/GPG-KEY-EUGridPMA-RPM-3
" > /etc/yum.repos.d/egi-trustanchors.repo
dnf install -y ca-policy-egi-core voms-clients-cpp globus-gridftp-server globus-gridftp-server-progs
firewall-cmd --zone=public --permanent --add-port=2811/tcp
firewall-cmd --zone=public --permanent --add-port=20000-21000/tcp
firewall-cmd --reload
firewall-cmd --list-all
echo '"/DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=faserdaq/CN=453330/CN=Robot: FASER DAQ" shifter' > /etc/grid-security/grid-mapfile
echo "
# globus-gridftp-server configuration file
\$GLOBUS_TCP_PORT_RANGE 20000,21000

# port
port 2811
restrict_paths /data
" > /etc/gridftp.conf
update-crypto-policies --set LEGACY  # to allow old SHA1 certificates
systemctl start globus-gridftp-server
systemctl enable globus-gridftp-server
systemctl status globus-gridftp-server
```

Altenative xrootd server instead of gridftp server:
```
dnf install xrootd xrootd-client
firewall-cmd --zone=public --add-port=1094/tcp --permanent
firewall-cmd --reload
cp /etc/grid-security/host* /etc/xrootd/
cp /etc/grid-security/grid-mapfile /etc/xrootd/
chown xrootd.xrootd /etc/xrootd/host*
echo "
all.sitename faser

all.role server

all.export /data
all.pidpath /run/xrootd
all.adminpath /var/spool/xrootd
oss.localroot /

xrd.port 1094
xrootd.chksum chkcgi adler32 crc32c

# TLS Configuration
xrd.tlsca certdir /etc/grid-security/certificates
xrd.tls /etc/xrootd/hostcert.pem /etc/xrootd/hostkey.pem

# Security via GSI
xrootd.seclib  libXrdSec.so
sec.protocol gsi -dlgpxy:1 -exppxy:=creds -cert:/etc/xrootd/hostcert.pem -key:/etc/xrootd/hostkey.pem -gridmap:/etc/xrootd/grid-mapfile
sec.protbind * only gsi

# HTTP and TPC configuration
xrd.protocol XrdHttp:8080 libXrdHttp.so
http.exthandler xrdtpc libXrdHttpTPC.so
ofs.tpc autorm echo fcreds gsi =X509_USER_PROXY logok scan all pgm /usr/bin/xrdcp
" > /etc/xrootd/xrootd-standalone.cfg

systemctl enable xrootd@standalone
systemctl start xrootd@standalone
```

Install gfal2 and plugins:
```
dnf install -y python3-gfal2 gfal2-plugin-xrootd
```

The FTS software should be installed.
```
dnf install -y fts-rest-client
```

Installation of DTO software:
```
dnf install -y python3-mysqlclient python3-sqlalchemy python3-dotenv python3-alembic

git clone --recurse-submodules https://:@gitlab.cern.ch:8443/faser/online/faser-dto.git
cd faser-dto

# add DB passwords to alembic.ini and .env files 
# if database is not setup:
# mysql -h dbod-faserdto.cern.ch -P 5505 -u admin -p
# mysql> create database faser_dto
# alembic upgrade head
```

Edit the .env to have right host name, file locations DB passwords, etc.

To run a test (as root):
```
export PYTHONPATH=$PWD/dto:PYTHONPATH
./faser-dto.py
```

Setup final logging and system service
```
mkdir /etc/dto.d
cp dto/doc/configuration-examples/dto-logging.ini /etc/dto.d/
#edit above file to turn off logging to console...
mkdir /var/log/dto
mkdir -p /usr/local/faser/
cp -a ../faser-dto /usr/local/faser/
cp faserdto.service /etc/systemd/system/

# allow systemd to run script - not sure this persists across boots?
/sbin/restorecon -v /usr/local/faser/faser-dto/run.sh
ausearch -c '(run.sh)' --raw | audit2allow -M my-runsh
semodule -X 300 -i my-runsh.pp

systemctl start faserdto
systemctl enable faserdto
systemctl status faserdto
```
