#!/usr/bin/env python

import logging
import os
import shutil
import sys
import time
from optparse import OptionParser

from config import validate_configuration, load_configuration_file
from models.Transfer import Transfer
from models.utils import get_db_session
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s - %(name)s (%(lineno)s) - '
                    '%(levelname)s: %(message)s',
                    datefmt='%Y.%m.%d %H:%M:%S')

def main(args):
    numTransfers=10
    log = logging.getLogger(__name__)
    if len(args):
        numTransfers=int(args[0])
    if not load_configuration_file(os.path.abspath(os.path.dirname(__file__))):
        exit(1)
    if not validate_configuration():
        exit(1)
    session = get_db_session(os.environ.get('db_user'),
                             os.environ.get('db_pass'),
                             os.environ.get('db_host'),
                             os.environ.get('db_port'),
                             os.environ.get('db_name')
                            )()
    print("Getting latest transfers")
    records = (
        session.query(Transfer)
#        .filter(Transfer.src_host != 'na62primitive')
#        .filter(Transfer.transfer_attempts == 1)
        .order_by(Transfer.created_at.desc())
        .limit(numTransfers)
    )
    for record in records:
        print "%s %s %s %10s %s %s %s %s " % (record.id, record.job_id, record.transfer_attempts,
                                     record.status, record.is_on_tape,record.updated_at, record.src_host,
                                     record.file_name)

if __name__ == '__main__':
    main(sys.argv[1:])

