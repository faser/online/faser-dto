#!/usr/bin/env python3
#
# Copyright (C) 2021 CERN
#
# FASER-DTO is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FASER-DTO is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DTO. If not, see <http://www.gnu.org/licenses/>.

import json
import gc
import glob
import logging.config
import os
import requests
import socket
import subprocess
import sys
import time
import traceback
import urllib3
from datetime import datetime

from config import load_configuration_file, validate_configuration, get_list_from_string

from controllers.DtoController import DtoController
from controllers.EosController import EosController
from controllers.FtsController import FtsController
from controllers.GracefulInterruptController import GracefulInterruptController
from controllers.SourceController import SourceController
from models.utils import get_db_session

influxDB=None
hostname=socket.gethostname()
if os.access("/etc/faser-secrets.json",os.R_OK):
    influxDB=json.load(open("/etc/faser-secrets.json"))
    urllib3.disable_warnings()

class XrootdSourceController(SourceController):
    def get_fts_source(self):
        """
        Return the FTS link on xrootd
        """
        return 'root://' + self.host + self.source

class FaserController(DtoController):
    # submission actually recreates sources, so have to overwrite them here
    def _submit(self, session, source, record=None, overwrite=False):
        if os.environ.get('source_protocol')=='xrootd':
            source = XrootdSourceController(source.host, source.get_source())
        return super()._submit(session, source, record, overwrite)

    def _get_new_files(self, source_dir):
        """
        Return a list of new files found in the source dir
        Files with size 0 will be deleted

        Requires correct ownership and file not in use

        Args:
          source_dir (str): The absolute path of the directory that contains the files to transfer

        Returns:
          list (:obj:`list` of :obj:`controllers.SourceController`): the list of new file
        """
        amount = 0
        source_dir = source_dir + '/'
        files = filter(os.path.isfile, glob.glob(source_dir + "*"))
        files = sorted(files, key=lambda x: os.path.getmtime(x))
        new_sources = []
        self.logger.info('Found '+str(len(files))+' files in '+source_dir)
        for new_file in files:
            if os.environ.get('source_protocol')=='xrootd':
                new_source = XrootdSourceController(self.host, new_file)
            else:
                new_source = SourceController(self.host, new_file)


            if new_source.get_owner() == os.environ.get(
                    'source_owner') and new_source.get_group() == os.environ.get('source_group'):
                # check if file opened by someone else
                rc=subprocess.Popen(["fuser",new_file],stdout=subprocess.PIPE).wait()
                if rc==0: 
                    self.logger.debug(new_file + ' is still being written to')
                    continue
                if '-000000-' in new_file or '-1000000000-' in new_file:
                    self.logger.info(new_file + ' is not a file with a legal run number')
                    continue

                amount += 1

                if new_source.is_size_zero():
                    self.logger.warning(new_file + ' deleted because file size is 0')
                    new_source.delete()
                    continue

                if amount <= self.new_files_limit:
                    new_sources.append(new_source)


        if influxDB:
            dtoInfo=f'dtoStatus,host={hostname} totalFiles={len(files)},pendingFiles={amount}'
            r=requests.post(f'https://dbod-faser-influx-prod.cern.ch:8080/write?db={influxDB["INFLUXDB"]}',
                            auth=(influxDB["INFLUXUSER"],influxDB["INFLUXPW"]),
                            data=dtoInfo,
                            verify=False)
            if r.status_code!=204:
                self.logger.error("Failed to post status influxdb: "+r.text)

        return new_sources

if __name__ == '__main__':
    DTO_LOGGING_CONFIGURATION_FILE = '/etc/dto.d/dto-logging.ini'
    LOGGING_FILE_EXIST = False
    if os.path.exists(DTO_LOGGING_CONFIGURATION_FILE):
        LOGGING_FILE_EXIST = True
        logging.config.fileConfig(DTO_LOGGING_CONFIGURATION_FILE)
    else:
        logging.basicConfig(level=logging.INFO,
                            format='%(asctime)s - %(name)s (%(lineno)s) - '
                                   '%(levelname)s: %(message)s',
                            datefmt='%Y.%m.%d %H:%M:%S'
                           )

    logger = logging.getLogger(__name__)
    if not LOGGING_FILE_EXIST:
        logger.warning('Missing logging config file: ' + DTO_LOGGING_CONFIGURATION_FILE)

    if not load_configuration_file(os.path.abspath(os.path.dirname(__file__))):
        exit(1)
    if not validate_configuration():
        exit(1)

    logger.info("Starting DTO")

    session = get_db_session(os.environ.get('db_user'),
                             os.environ.get('db_pass'),
                             os.environ.get('db_host'),
                             os.environ.get('db_port'),
                             os.environ.get('db_name')
                            )()
    logger.info('Database connection established')


    minutes_of_life = int(os.environ.get('minutes_of_life'))
    check_tape = bool(int(os.environ.get('check_tape')))
    sleep_time = int(os.environ.get('sleep_time'))
    resubmit_after_minutes = int(os.environ.get('resubmit_after_minutes'))

    try:
        jump_cleanup_time = int(os.environ.get('jump_cleanup_time'))
    except ValueError as e:
        jump_cleanup_time = 1440
        logger.info("No value set for jump_cleanup_time. Default value of 1440 will be used.")
    jump_endpoint = os.environ.get('jump_protocol_base')

    fts_instance = FtsController(os.environ.get('endpoint'))
    if jump_endpoint != "":
        eos_storage = EosController(endpoint=jump_endpoint)
        dto = FaserController(fts_instance, os.environ.get('host'), eos_storage)
    else:
        dto = FaserController(fts_instance, os.environ.get('host'))

    file_dirs = get_list_from_string(os.environ.get('file_dirs'))

    with GracefulInterruptController() as handler:
        start_date = datetime.utcnow()
        record_success_tot = 0
        record_fail_tot = 0
        submit_success_tot = 0
        submit_fail_tot = 0
        transfer_finished_tot = 0
        transfer_failed_tot = 0
        files_deleted_tot = 0
        transfer_resubmitted_tot = 0

        cycle_count = 0
        count_since_beginning = 0

        while True:
            try:
                # the purpose of this query is to check the db status
                # in case of failure the code below will be skipped until the connection is back
                result = session.execute('SELECT version()').fetchall()

                # Look for new file in every dir
                for file_dir in file_dirs:
                    if not os.path.isdir(file_dir):
                        logger.error(file_dir + " doesn't exist")
                        continue

                    (record_success, record_fail) = dto.record_files(session, file_dir)
                    record_success_tot += record_success
                    record_fail_tot += record_fail

                # Create a new record for file that need to be resubmitted
                transfer_resubmitted_tot += dto.do_resubmit(session,
                                                            resubmit_after_minutes)

                # Submit new files and the one that needs resubmission
                (submit_success, submit_fail) = dto.submit_files(session)
                submit_success_tot += submit_success
                submit_fail_tot += submit_fail

                (transfer_finished, transfer_failed) = dto.fetch_transfer_status(session)
                transfer_finished_tot += transfer_finished
                transfer_failed_tot += transfer_failed

                if check_tape:
                    interval_between_checks_minutes = 5
                    dto.fetch_tape_status(session, interval_between_checks_minutes)

                files_deleted_tot += dto.delete_files(session, minutes_of_life, int(check_tape))

                for file_dir in file_dirs:
                    if not os.path.isdir(file_dir):
                        logger.error(file_dir + " doesn't exist")
                        continue
                    # occupancy based selection
                    dto.delete_over_threshold(session, file_dir, int(check_tape))

                dto.cleanup_jump_server(session, jump_cleanup_time)

                logger.info("DTO running since UTC " + str(start_date))
                logger.info("Files recorded: " + str(record_success_tot))
                logger.info("Files recorded fail: " + str(record_fail_tot))
                logger.info("Submit fail: " + str(submit_fail_tot))
                logger.info("Submitted success: " + str(submit_success_tot))
                logger.info("Transfers FINISHED: " + str(transfer_finished_tot))
                logger.info("Transfers FAILED:  " + str(transfer_failed_tot))
                logger.info("Resubmissions: " + str(transfer_resubmitted_tot))
                logger.info("Files deleted: " + str(files_deleted_tot))

                if influxDB:
                    eosUsed=0
                    eosTotal=0
                    eosPercentage=0
                    if jump_endpoint != "":
                        jump_base = os.environ.get('jump_base_destination')
                        cmd=f"EOS_MGM_URL={jump_endpoint} eos quota {jump_base}"
                        try:
                            proc=subprocess.run(cmd,
                                                stdout=subprocess.PIPE,
                                                stderr=subprocess.PIPE,
                                                timeout=20,
                                                shell=True)
                            result=proc.stdout.decode().split('\n')
                        except subprocess.TimeoutExpired:
                            logger.error(f"Got timeout reading quota with cmd={cmd}")
                            result=["\n"]*10
                        if len(result)>3:
                            result=result[-3]
                            if result.startswith(' project'):
                               words=result.split()
                               try:
                                   eosUsed=float(words[3])
                                   eosTotal=float(words[9])
                                   eosPercentage=float(words[13])
                               except ValueError:
                                   logger.error("Could not decode EOS quota results")
                            else:
                                logger.error("Could not decode EOS quota output")
                        

                    dtoInfo=f'dtoInfo,host={hostname} filesRecorded={record_success_tot},filesRecordedFail={record_fail_tot},submitFail={submit_fail_tot},submitSuccess={submit_success_tot},transferFinished={transfer_finished_tot},transferFailed={transfer_failed_tot},resubmissions={transfer_resubmitted_tot},filesDeleted={files_deleted_tot},eosUsed={eosUsed},eosTotal={eosTotal},eosPercentage={eosPercentage}'
                    r=requests.post(f'https://dbod-faser-influx-prod.cern.ch:8080/write?db={influxDB["INFLUXDB"]}',
                                    auth=(influxDB["INFLUXUSER"],influxDB["INFLUXPW"]),
                                    data=dtoInfo,
                                    verify=False)
                    if r.status_code!=204:
                        self.logger.error("Failed to post status influxdb: "+r.text)
                cycle_count += 1
                count_since_beginning += 1

                if count_since_beginning * sleep_time > 60 * 60:
                    logger.info("Scheduled exit ")
                    # automatic exit after an hour due to memory leak
                    # the service manager will restart it
                    sys.exit(1)

                if cycle_count * sleep_time > 60 * 10:
                    collected = gc.collect()
                    cycle_count = 0
                    logger.info("Garbage collector: collected %d objects.", collected)

                time.sleep(sleep_time)
            except Exception as e:
                # that was the only way to catch this exception
                # https://mail.python.org/pipermail/python-list/2008-January/483946.html
                if e.__class__.__name__.find('OperationalError') != -1:
                    session.rollback()
                    wait_before_reconnect = 7
                    logger.error(
                        "Database connection lost.. waiting " + str(
                            wait_before_reconnect) + "s before reconnecting..")
                    time.sleep(wait_before_reconnect)
                else:
                    # unknown exception
                    logger.error(e.__class__.__name__)
                    logger.error(traceback.print_exc())
                    exit(1)

            if handler.interrupted:
                logger.info('DTO interrupped!')
                time.sleep(1)
                break
