#!/usr/bin/env python3

import os
import sys

import json
import requests

def main(args):
    runtype=args[0]
    hosts=args[1:]
    r = requests.get(f'http://faser-runnumber.web.cern.ch/RunList')	
    runlist=r.json()
    selected=[]
    for run in runlist:
        if run['type']==runtype and run['host'].split('.')[0] in hosts:
            selected.append(run['runnumber'])
    selected.sort()
    print(" ".join([str(run) for run in selected]))

if __name__ == '__main__':
    main(sys.argv[1:])
