#!/usr/bin/env python3

import os
import sys
import time

def main(args):
    if len(args)<2:
        print("Usage: ./moveOld.py <orignal EOS dir> <run1> [run2] ...")
        sys.exit(1)
    loc=args[0]
    runs=[int(run) for run in args[1:]]
    for run in runs:
        print(f"Working on run {run}")
        eosdir=f"/eos/project/f/faser-commissioning/{loc}/Run-{run:06d}/"
        files=os.listdir(eosdir)
        print(f" Found {len(files)} files")
        for name in files:
            if not name.endswith(".raw"):
                print("Skipping: "+name)
                continue
            rc=os.system(f"xrdcp root://eosproject-f.cern.ch/{eosdir}/{name} /data")
            if rc!=0:
                print(f"Failed to copy {name} - bailing out")
                sys.exit(1)
        for name in files:
            if not name.endswith(".raw"):
                continue
            rc=os.system(f"sudo chown shifter.shifter /data/{name}")
            if rc!=0:
                print(f"Failed to change owner of {name} - bailing out")
                sys.exit(1)
        print(" Files copied - now wait a bit")
        time.sleep(len(files)*8)

if __name__ == '__main__':
    main(sys.argv[1:])
