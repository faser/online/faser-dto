#!/usr/bin/env python3

import os
import subprocess
import sys
import time

def getHash(server,name):
    rc,hashline = subprocess.getstatusoutput(f"xrdfs {server} query checksum {name}")
    if rc:
        print(f"Failed to get hash for {name}")
        #raise Exception(f"Failed to get hash for {name}")
        return "1234"
    return hashline.split()[1]

def main(args):
    oldserver="eosproject-f.cern.ch"
    newserver="eospublic.cern.ch"
    oldarea=args[0]
    newarea=args[1]
    doTS=False
    repair=False
    if len(args)>2: doTS=True
    if len(args)>3: repair=True
    olddir=f"/eos/project/f/faser-commissioning/{oldarea}"
    newdir=f"/eos/experiment/faser/raw/{newarea}"
    for run in os.listdir(olddir):
        if not run.startswith("Run-00"): continue
        runno=f"{int(run[4:]):06d}"
        print(f"Checking run {runno}")
        if not os.access(f"{newdir}/{runno}",os.F_OK):
            rc,size = subprocess.getstatusoutput(f"du -hs {olddir}/{run}")
            print(f" Run {runno} was not copied ({size.split()[0]})")
            continue
        if doTS:
            ts=time.localtime(os.stat(f"{olddir}/{run}").st_mtime)
            ts=f"{ts.tm_year}{ts.tm_mon:02d}{ts.tm_mday:02d}{ts.tm_hour:02d}{ts.tm_min:02d}.{ts.tm_sec:02d}"
            rc=os.system(f"touch -m -t {ts} {newdir}/{runno}")
            if rc: 
                print(f"ERROR: failed to update timestamp for {runno}")
        for fname in os.listdir(f"{olddir}/{run}"):
            if not fname.endswith(".raw"):
                print(f"Skipping {fname}")
                continue
            oldname=f"{olddir}/{run}/{fname}"
            oldhash=getHash(oldserver,oldname)
            if oldhash=="00000001":
                #print(f" {fname} is empty file - not copied")
                continue
            newname=f"{newdir}/{runno}/{fname}"
            newhash=getHash(newserver,newname)
            if newhash!=oldhash:
                print(f"ERROR in hash for {newname}: {oldhash}!={newhash}")
                if repair:
                    os.system(f"cp -a {oldname} {newname}")
                    time.sleep(10)
                    newhash=getHash(newserver,newname)
                    if newhash!=oldhash:
                        print(f"FATAL in repairing {newname}: {oldhash}!={newhash}")
                        exit(1)
            if newhash==oldhash and doTS:
                ts=time.localtime(os.stat(oldname).st_mtime)
                tsnew=time.localtime(os.stat(newname).st_mtime)
                if ts!=tsnew:
                    print(f"Different timestamp for {newname}: {ts} vs {tsnew}")
                    ts=f"{ts.tm_year}{ts.tm_mon:02d}{ts.tm_mday:02d}{ts.tm_hour:02d}{ts.tm_min:02d}.{ts.tm_sec:02d}"
                    rc=os.system(f"touch -m -t {ts} {newname}")
                    if rc: 
                        print(f"ERROR: failed to update timestamp for {newname}")
                    else:
                        time.sleep(2)
                        newhash=getHash(newserver,newname)
                        if newhash!=oldhash:
                            print(f"FATAL in updating TS {newname}: {oldhash}!={newhash}")
                            exit(1)
                    
if __name__ == '__main__':
    main(sys.argv[1:])
